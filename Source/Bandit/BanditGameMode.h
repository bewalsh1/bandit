// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "UsableActor.h"
#include "SpawnVolume.h"
#include "Guard.h"
#include "BanditGameMode.generated.h"

#define COLLISION_PROJECTILE ECC_GameTraceChannel1

// enum to store the current state of gameplay
UENUM(BlueprintType)
enum class EBanditPlayState : uint8{
	EGameWon UMETA(DisplayName="Game Won"), 
	EPlaying UMETA(DisplayName="Playing"),
	EGameover UMETA(DisplayName="Game Over"),
	EUnknown UMETA(DisplayName="Unknown")
};

/**
 * 
 */
UCLASS()
class BANDIT_API ABanditGameMode : public AGameMode{
	GENERATED_BODY()
	
private:
	

	// Internal handling of new states
	void HandleNewState(EBanditPlayState NewState);

	UClass* guardClass;

public:
	// Constructor
	ABanditGameMode(const FObjectInitializer& ObjectInitializer);

	// Enum instance of available game states
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
	EBanditPlayState CurrentState;

	// Override basic functionality
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	// Handle Game States
	EBanditPlayState GetCurrentState() const;
	void SetCurrentState(EBanditPlayState NewState);

	// Track found documents
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameStats)
	int32 docCount;
	AUsableActor* currentDocument;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AlertSystem)
	float alertLevel;

	void SpawnDocument();

	TArray<AGuard*> guardsList;
	void SpawnGuards();

	void AssignWaypointsToGuards();

protected:
	// after all game elements are created
	virtual void PostInitializeComponents() override;
};

FORCEINLINE EBanditPlayState ABanditGameMode::GetCurrentState() const{
	return CurrentState;
}