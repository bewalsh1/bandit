// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup.h"
#include "DocumentPickup.generated.h"

/**
 * INITIAL CODE IS JUST FOLLOWING TUTORIAL UNRELATED TO GAME. REMOVE THIS COMMENT WHEN THAT CODE HAS BEEN REMOVED
 */
UCLASS()
class BANDIT_API ADocumentPickup : public APickup{
	GENERATED_BODY()

public:
	// Constructor declaration
	ADocumentPickup(const FObjectInitializer& ObjectInitializer);
	
	/* Doesn't need UFUNCTION modifier, picks it up from function being overridden */
	/* Override the OnPickedUp function (use Implementation because this is a BlueprintNativeEvent) */
	void OnPickedUp_Implementation() override;

	
};
