// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "BanditController.h"
#include "BanditGameMode.h"
#include "BanditHUD.h"
#include "DocumentPickup.h"


BANDIT_API ADocumentPickup::ADocumentPickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){

}

void ADocumentPickup::OnPickedUp_Implementation(){
	// Call the parent implementation of OnPickedUp
	Super::OnPickedUp_Implementation();
}
