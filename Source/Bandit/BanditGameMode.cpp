// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "BanditGameMode.h"
#include "BanditController.h"
#include "Kismet/GameplayStatics.h"
#include "BanditHUD.h"

ABanditGameMode::ABanditGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL){
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	docCount = 0;

	// set the default HUD class
	HUDClass = ABanditHUD::StaticClass();
	
	static ConstructorHelpers::FObjectFinder<UBlueprint> HUDObj(TEXT("/Game/Blueprints/BanditHUD"));
	if (HUDObj.Object != NULL){
		HUDClass = (UClass*)HUDObj.Object->GeneratedClass;
	}

	/*guardClass = AGuard::StaticClass();
	static ConstructorHelpers::FObjectFinder<UBlueprint> guardObj(TEXT("/Game/Character/Guard/GuardCharacter"));
	if (guardObj.Object != NULL){
		guardClass = Cast<UClass>(guardObj.Object->GeneratedClass);
	}*/
}

void ABanditGameMode::PostInitializeComponents(){
	Super::PostInitializeComponents();

	//this->SpawnGuards();
	//this->AssignWaypointsToGuards();
}

void ABanditGameMode::Tick(float DeltaSeconds){
	ABanditController* bandit = Cast<ABanditController>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void ABanditGameMode::HandleNewState(EBanditPlayState NewState){
	switch (NewState){
		// Game is playing as Normal
		case EBanditPlayState::EPlaying:{

		}
		break;
		// Game State Won
		case EBanditPlayState::EGameWon:{

		}
		break;
		// Game Over state
		case EBanditPlayState::EGameover:{

		}
		break;
		// Unknown and Default states
		case EBanditPlayState::EUnknown:
		default:{
			// do nothing
		}
		break;
	}
}

void ABanditGameMode::SetCurrentState(EBanditPlayState NewState){
	CurrentState = NewState;

	HandleNewState(NewState);
}

void ABanditGameMode::BeginPlay(){
	Super::BeginPlay();

	SetCurrentState(EBanditPlayState::EPlaying);
}

void ABanditGameMode::SpawnGuards(){
	TActorIterator<ATargetPoint> targetPointsItr(GetWorld());

	UWorld* world = GetWorld();
	if (world){
		while (targetPointsItr){
			if (targetPointsItr->ActorHasTag("GuardWaypoint") && targetPointsItr->ActorHasTag("GuardSpawn")){
				const FVector newLocation = targetPointsItr->GetActorLocation();
				const FRotator newRotation = FRotator::ZeroRotator;
				const FActorSpawnParameters parameters;
				AGuard* newGuard = SpawnBP<AGuard>(GetWorld(), guardClass, newLocation, newRotation);
				UE_LOG(YourLog, Warning, TEXT("Spawned New Guard"));

				TArray<FName> tags = targetPointsItr->Tags;
				for (int i = 0; i < tags.Num(); i++){
					FString tag = tags[i].ToString();
					if (tag.Contains("GuardGroup")){
						newGuard->tagGroup = tag;
						newGuard->currentWaypoint = *targetPointsItr;
						newGuard->waypointPos = 0;
					}
				}

				guardsList.Add(newGuard);
			}
			++targetPointsItr;
		}
	}
}

void ABanditGameMode::AssignWaypointsToGuards(){
	TActorIterator<ATargetPoint> targetPointsItr(GetWorld());

	UWorld* world = GetWorld();
	while (targetPointsItr){
		if (targetPointsItr->ActorHasTag("GuardWaypoint")){
			for (int i = 0; i < guardsList.Num(); i++){
				if (targetPointsItr->ActorHasTag(FName(*guardsList[i]->tagGroup))){
					guardsList[i]->waypointsList.Add(*targetPointsItr);
				}
			}
		}
		++targetPointsItr;
	}
}

void ABanditGameMode::SpawnDocument(){
	docCount++;
	UE_LOG(YourLog, Warning, TEXT("Document Count %d"), docCount);
	if (docCount < 4){
		TActorIterator<ATargetPoint> targetPointsItr(GetWorld());
		
		UWorld* world = GetWorld();
		if (world){
			while (targetPointsItr){
				if (targetPointsItr->ActorHasTag("DocumentSpawn")){
					const FVector newLocation = targetPointsItr->GetActorLocation();
					const FRotator newRotation = FRotator::ZeroRotator;
					const FActorSpawnParameters parameters;
					currentDocument = Cast<AUsableActor>(world->SpawnActor(AUsableActor::StaticClass(), &newLocation, &newRotation, parameters));
				
					targetPointsItr->Destroy();
					break;
				}
				++targetPointsItr;
			}
		}
	}
	else{
		SetCurrentState(EBanditPlayState::EGameWon);
	}
}