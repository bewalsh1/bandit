// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DialogueActor.generated.h"

/**
 * 
 */
UCLASS()
class BANDIT_API ADialogueActor : public AActor
{
	GENERATED_BODY()
	
public:
	ADialogueActor(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite, Category = Sound)
	int32 DialogueNumber;

	
	
};
