#pragma once

#include "BanditController.h"
#include "UsableActor.h"
#include "GameFramework/HUD.h"
#include "BanditHUD.generated.h"

/**
* Enum for listing button types
* Used to determine action of clicked button
* Each button's type is exposed through Blueprint via FHUDButton struct
*/
UENUM(BlueprintType)
enum class EButtonTypes : uint8{
	MM_Restart UMETA(DisplayName = "Restart Game"),
	MM_Exit UMETA(DisplayName = "Exit Game"),
	CNFRM_YES UMETA(DisplayName = "Confirm Yes"),
	CNFRM_NO UMETA(DisplayName = "Confirm No")
};

/**
* Struct that contains attributes of a button
* Most variables exposed through Blueprint
* XMin etc are used for checking if click is in button, set in PostInitializeComponents
*/
USTRUCT(BlueprintType, Blueprintable, Category = HUDButtons)
struct FHUDButton{
	GENERATED_USTRUCT_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	EButtonTypes buttonType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD2DElements)
	UTexture2D* btnBackground;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	FString btnText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	FLinearColor textColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float xPos; // xPos of actual button
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float yPos; // yPos of actual button
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float btnTextXPos; // xPos of text inside button
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float btnTextYPos; // yPos of text inside button
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float btnWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD_Buttons)
	float btnHeight;



	// Using for cursor checking, set in draw button function
	float xMin;
	float yMin;
	float xMax;
	float yMax;
};

/**
 * Central processing for all HUD functions
 */
UCLASS()
class BANDIT_API ABanditHUD : public AHUD
{
	GENERATED_BODY()

public:
	ABanditHUD(const FObjectInitializer& ObjectInitializer);

// Fonts
// Recommended to start with large size (e.g. 36) and scale down as needed
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BanditHUD)
	UFont* verdanaFont;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = BanditHUD)
	float defaultFontScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		UTexture* MiniMapTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		float MapWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		float MapHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		FVector WorldCenter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		FVector WorldExtent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		float MiniMapMargin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		float UIScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Minimap)
		FVector2D OffsetMap;

	//Current location of Bandit, used in updating minimap, to be set as he moves
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Minimap)
		FVector PlayerLocationHUD;

// Cursor
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	UTexture2D* cursorStandard;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	UTexture2D* cursorHovering;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	int32 cursorXPosOffset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	int32 cursorYPosOffset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	int32 cursorWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements)
	int32 cursorHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Standard Texture Init X")
	int32 cursorStandardTexXPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Standard Texture Init Y")
	int32 cursorStandardTexYPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Hover Texture Init X")
	int32 cursorHoverTexXPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Hover Texture Init Y")
	int32 cursorHoverTexYPos;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Standard Width In Texture")
	int32 cursorStandardTexWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Standard Height In Texture")
	int32 cursorStandardTexHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Hover Width In Texture")
	int32 cursorHoverTexWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cursor2DElements, DisplayName = "Cursor Hover Height In Texture")
	int32 cursorHoverTexHeight;

	FVector2D MouseLocation;
	void DrawCursor();

// Buttons
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDButtons)
	TArray<FHUDButton> buttonsMain;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDButtons)
	TArray<FHUDButton> buttonsConfirm;
	
	void InitButtonParameters(TArray<FHUDButton>& btnArray);
	void CheckAllButtons();
	int32 GetClickedButtonType(const TArray<FHUDButton>& btnArray);
	void ExecuteButtonSelection(const TArray<FHUDButton>& btnArray);

	const FHUDButton* curCheckButton;
	int32 clickedButtonType;
	int32 activeType;
	bool bInButton;

// Draw Calls
public:
	// probably should be renamed / maybe split into two (more?) variables for the backgrounds
	// EVEN BETTER MAY BE SPECIFYING A STRUCT AND EXPOSING TO BLUEPRINT, ALLOWS ARTISTS TO SPECIFY ALL THAT JUNK
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DrawOptions)
	UMaterialInterface* materialBackground; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = DrawOptions)
	bool bDrawHUD;

	bool bConfirmDialog;
	bool bMainMenu;

	void DrawDialogs();
	void DrawMainMenu();
	void DrawConfirm();
	void DrawButtons(TArray<FHUDButton>& btnArray);

	virtual void Tick(float DeltaSeconds) override;

// Core 
public:
	APlayerController* playerController;
	ABanditController* banditController;
	void PlayerInputChecks();
	static void DocumentFoundAlert(AUsableActor* document);

protected:
	void Reset();
	virtual void DrawHUD() override;

	void DrawMiniMap();

	// after all game elements are created
	virtual void PostInitializeComponents() override;


	// Pause the game and open Citizen Clues Hints
public:
	void Pause();
	// Slate Widgets
	TSharedPtr<class SCitizenCluesHints> CitizenCluesHints;


}; 


// Utility

// TO CHANGE
// Stop Camera From Moving With Mouse
/*FORCEINLINE void SetCursorMoveOnly(bool cursorOnly){
if(!banditController){ return; }
banditController->SetIgnoreLookInput(cursorOnly);
}*/