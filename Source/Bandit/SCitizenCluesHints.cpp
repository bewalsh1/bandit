// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "BanditController.h"
#include "SCitizenCluesHints.h"
#include "STextBlock.h"


//BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SCitizenCluesHints::Construct(const FArguments& InArgs)
{
	/*
	ChildSlot
	[
		// Populate the widget
	];
	*/
	OwnerHUD = InArgs._OwnerHUD;
	OwnerController = InArgs._OwnerController;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/////If the code below doesn't look like C++ to you it's because it (sort-of) isn't,
	/////Slate makes extensive use of the C++ Prerocessor(macros) and operator overloading,
	/////Epic is trying to make our lives easier, look-up the macro/operator definitions to see why.
	ChildSlot
		.VAlign(VAlign_Fill)
		.HAlign(HAlign_Fill)
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot().FillHeight(3)
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Fill)
			[
				SNew(SBox).VAlign(VAlign_Fill).HAlign(HAlign_Fill)
			]
			+ SVerticalBox::Slot().FillHeight(10)
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Fill)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().Padding(10, 5).FillWidth(1)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Left)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().Padding(5, 5)
					.FillHeight(10).MaxHeight(750)
					[
						SNew(SScrollBox)
						+ SScrollBox::Slot()
						[
							SNew(STextBlock)
							.ShadowColorAndOpacity(FLinearColor::Black)
							.ColorAndOpacity(FLinearColor::White)
							.ShadowOffset(FIntPoint(-1, 1))
							.Font(FSlateFontInfo("Veranda", 24))
							.Text(FText::FromString("Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque in massa vel arcu lacinia vestibulum.Nunc ut dui rutrum, imperdiet ligula quis, venenatis turpis.Nam rhoncus molestie libero eu feugiat.Nullam ac nisi dictum, pellentesque neque accumsan, convallis tortor.Praesent quis faucibus orci.In consequat, quam a egestas auctor, risus eros dapibus tortor, non pretium urna urna ac mi.Integer et convallis urna.Ut ullamcorper lorem ut pulvinar fringilla.Ut vestibulum felis orci, sit amet aliquam nulla semper sit amet.Etiam et arcu ullamcorper, gravida elit eu, faucibus purus.Sed a sollicitudin leo.Quisque sit amet neque vitae arcu volutpat dictum sit amet sit amet nisi.Phasellus placerat nisl ac neque cursus, id tristique tellus semper."))
							.AutoWrapText(true)
						]
					]
				]
				+ SHorizontalBox::Slot().Padding(10, 5).FillWidth(1)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Fill)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot().Padding(5, 5)
					.FillHeight(10).MaxHeight(750)
					[
						SNew(SScrollBox)
						+ SScrollBox::Slot()
						[
							SNew(SMultiLineEditableTextBox)
							.Font(FSlateFontInfo("Veranda", 24))
							.HintText(FText::FromString("Add notes here: abcdefghijklmnopqrstuvwxyz\n"))
							
							.WrapTextAt(600.)
							.Cursor(Cursor)
						]
					]
				]
			]
			+ SVerticalBox::Slot().FillHeight(1)
			.VAlign(VAlign_Center)
			.HAlign(HAlign_Fill)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().Padding(10, 5)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Fill)
				[
					SNew(SButton).HAlign(HAlign_Center)
					.ButtonColorAndOpacity(FLinearColor::Gray)
					[
						SNew(STextBlock)
						.ShadowColorAndOpacity(FLinearColor::Black)
						.ColorAndOpacity(FLinearColor::White)
						.ShadowOffset(FIntPoint(-1, 1))
						.Font(FSlateFontInfo("Veranda", 32))
						.Text(FText::FromString("Previous"))
					]
				]
				+ SHorizontalBox::Slot().Padding(10, 5)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Fill)
				[
					SNew(SButton).HAlign(HAlign_Center)
					.ButtonColorAndOpacity(FLinearColor::Gray)
					[
						SNew(STextBlock)
						.ShadowColorAndOpacity(FLinearColor::Black)
						.ColorAndOpacity(FLinearColor::White)
						.ShadowOffset(FIntPoint(-1, 1))
						.Font(FSlateFontInfo("Veranda", 32))
						.Text(FText::FromString("Next"))
					]
				]
				+ SHorizontalBox::Slot().Padding(10, 5)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Fill)
				[
					SNew(SButton).HAlign(HAlign_Center)
					.ButtonColorAndOpacity(FLinearColor::Gray)
					.OnClicked_Lambda([]()->FReply{ GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Press Tab to Continue!"));
					GEngine->GameViewport->RemoveAllViewportWidgets();
					return FReply::Handled(); })
					[
						SNew(STextBlock)
						.ShadowColorAndOpacity(FLinearColor::Black)
						.ColorAndOpacity(FLinearColor::White)
						.ShadowOffset(FIntPoint(-1, 1))
						.Font(FSlateFontInfo("Veranda", 32))
						.Text(FText::FromString("Continue"))
					]
				]
				+ SHorizontalBox::Slot().Padding(10, 5)
				.VAlign(VAlign_Center)
				.HAlign(HAlign_Fill)
				[
					SNew(SButton).HAlign(HAlign_Center)
					.ButtonColorAndOpacity(FLinearColor::Gray)
					.OnClicked_Lambda([]()->FReply{ GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Press Tab to Continue!\nPress Esc to Exit!"));
					GEngine->GameViewport->RemoveAllViewportWidgets();
					return FReply::Handled(); })
					[
						SNew(STextBlock)
						.ShadowColorAndOpacity(FLinearColor::Black)
						.ColorAndOpacity(FLinearColor::White)
						.ShadowOffset(FIntPoint(-1, 1))
						.Font(FSlateFontInfo("Veranda", 32))
						.Text(FText::FromString("Exit"))
					]
				]
			]
		];
}
//END_SLATE_FUNCTION_BUILD_OPTIMIZATION

SCitizenCluesHints::SCitizenCluesHints(){

}

const void SCitizenCluesHints::ContinueButton(){

}