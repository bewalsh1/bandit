// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "UsableActor.h"


AUsableActor::AUsableActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	FName path = FName(TEXT("/Game/Modelsd/Scenery/Pieces/Letter_v1.Letter_v1"));
	
	UStaticMesh* docMesh = LoadMeshFromPath(path);
	if (docMesh){
		this->GetStaticMeshComponent()->SetStaticMesh(docMesh);
		this->GetStaticMeshComponent()->bGenerateOverlapEvents = true;
	}
}