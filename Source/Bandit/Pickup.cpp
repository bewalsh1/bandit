// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "Pickup.h"

BANDIT_API APickup::APickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	// The pickup is valid when it is created
	isActive = true;

	// Create the root SphereComponent to handle the pickup''s collision
	BaseCollisionComponent = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("BaseSphereComponent"));

	// Set the SphereComponent as the root component
	RootComponent = BaseCollisionComponent;

	// Create the static mesh component
	PickupMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("PickupMesh"));

	// Turn physics on for the static mesh
	PickupMesh->SetSimulatePhysics(true);

	// Attach the StaticMeshComponent to the root component
	PickupMesh->AttachTo(RootComponent);
}


/* _Implementation because it's a blueprint native event, without the _Implementation the implementation is a backup to whatever Blueprint normally checks */
void APickup::OnPickedUp_Implementation(){
	// There is no default behavior for a Pickup when it is picked up.
}