// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Guard.h"
#include "AIModule.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "GuardController.generated.h"

/**
 * 
 */
UCLASS()
class BANDIT_API AGuardController : public AAIController
{
	GENERATED_BODY()

	APawn* pawn;

	
	
public:
	AGuardController(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category="Utility")
	void LoadOntoBlackboard();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Guard")
	AGuard* guard;
	
protected:
	// after all game elements are created
	virtual void PostInitializeComponents() override;

	virtual void Possess(APawn* inPawn) override;
};
