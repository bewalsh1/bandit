// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "GuardController.h"

AGuardController::AGuardController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){

}

void AGuardController::Possess(APawn* inPawn){
	Super::OnPossess(inPawn);

	pawn = inPawn;
	
}

void AGuardController::PostInitializeComponents(){
	Super::PostInitializeComponents();

	/*UE_LOG(YourLog, Warning, TEXT("Inside Guard PostInitialize"));

	
	if (this->GetPawn()){
		UE_LOG(YourLog, Warning, TEXT("Inside Guard PostInitialize %s"), *this->GetPawn()->GetName());
	}*/

	// Get the currently possessed pawn
	
	/*if (this->GetCharacter()->GetClass() == AGuard::StaticClass()){
		guard = Cast<AGuard>(this->GetCharacter());
	}
	
	if (guard){
		for (int i = 0; i < guard->waypointsList.Num(); i++){
			UE_LOG(YourLog, Warning, TEXT("Waypoint is %s"), *guard->waypointsList[i]->GetName());
		}
	}*/
}

void AGuardController::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);

}

void AGuardController::BeginPlay(){
	Super::BeginPlay();
}

void AGuardController::LoadOntoBlackboard(){
	UE_LOG(YourLog, Warning, TEXT("Inside Load Blackboard"));

	if (pawn->GetClass()->GetName() == "GuardCharacter_C"){
		//UBlackboardComponent* blackboard = this->BrainComponent->GetBlackboardComponent();
		//if (blackboard){
		//	UE_LOG(YourLog, Warning, TEXT("You got the blackboard: %s"), *blackboard->GetName());

			guard = Cast<AGuard>(pawn);
			/*if (guard){
			UE_LOG(YourLog, Warning, TEXT("Inside Begin Play %s Num: %d"), *pawn->GetName(), guard->waypointsList.Num());
			for (int i = 0; i < guard->waypointsList.Num(); i++){
			UE_LOG(YourLog, Warning, TEXT("Waypoint is %s"), *guard->waypointsList[i]->GetName());
			}
			}*/
		//}
	}
}