// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UsableActor.h"
#include "GameFramework/Character.h"
#include "Guard.generated.h"

/**
 * 
 */
UCLASS()
class BANDIT_API AGuard : public ACharacter{
	GENERATED_BODY()
	
	virtual void BeginPlay() override;

public:
	AGuard(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = WaypointTags)
	FString tagGroup;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = WaypointTags)
	TArray<ATargetPoint*> waypointsList;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = WaypointTags)
	ATargetPoint* currentWaypoint;

	int32 waypointPos;



protected:
	// after all game elements are created
	virtual void PostInitializeComponents() override;
};

template <typename ObjType>
static FORCEINLINE ObjType* SpawnBP(UWorld* world, UClass* blueprint, const FVector& loc, const FRotator& rot, 
									const bool bNoCollisionFail = true, AActor* Owner = NULL, APawn* instigator = NULL){
	if (!world){ return NULL; }
	if (!blueprint) { return NULL; }

	FActorSpawnParameters spawnInfo;
	spawnInfo.bNoCollisionFail = bNoCollisionFail;
	spawnInfo.Owner = Owner;
	spawnInfo.Instigator = instigator;
	spawnInfo.bDeferConstruction = false;

	return world->SpawnActor<ObjType>(blueprint, loc, rot, spawnInfo);
	
}
