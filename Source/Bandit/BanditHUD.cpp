// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "BanditHUD.h"
#include "BanditGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "SCitizenCluesHints.h"
#include "SWeakWidget.h"

#define CANVAS_WHITE if(Canvas){Canvas->SetDrawColor(FColor_White);}

ABanditHUD::ABanditHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	bDrawHUD = false;
	defaultFontScale = 0.7;

	// States
	bConfirmDialog = false;
	bMainMenu = true;
}

// Initializes variables used for click bounds checking
void ABanditHUD::InitButtonParameters(TArray<FHUDButton>& btnArray){
	for (int i = 0; i < btnArray.Num(); i++){
		btnArray[i].xMin = btnArray[i].xPos;
		btnArray[i].yMin = btnArray[i].yPos;
		btnArray[i].xMax = btnArray[i].xPos + btnArray[i].btnWidth;
		btnArray[i].yMax = btnArray[i].yPos + btnArray[i].btnHeight;
	}
}

void ABanditHUD::PostInitializeComponents(){
	Super::PostInitializeComponents();

	playerController = GetOwningPlayerController();
	banditController = Cast<ABanditController>(playerController);

	InitButtonParameters(buttonsMain);
	InitButtonParameters(buttonsConfirm);

}

void ABanditHUD::DrawDialogs(){
	DrawMainMenu();
	if (bConfirmDialog){ DrawConfirm(); }
}

void ABanditHUD::DrawMainMenu(){
	// Background - uncomment once main menu mapped to pause menu (rename main menu vars to pause menu
	// DrawMaterialSimple(materialBackground, 10, 10, 256, 512, 1.3);

	// Menu Title

	// Draw Buttons
	DrawButtons(buttonsMain);
}

void ABanditHUD::DrawConfirm(){
	// Blue rect with alpha 50%
	// replace with an actual confirmation box
	// expose stuff to blueprint
	// DrawBanditRect(Canvas->SizeX/2 - 100, Canvas->SizeY/2 - 50, 20, 100, FLinearColor(0,0,1,0.2333));

	// Confirm Title

	// Draw Buttons
	DrawButtons(buttonsConfirm);
}

// Draws a set of buttons passed in as a parameters
void ABanditHUD::DrawButtons(TArray<FHUDButton>& btnArray){
	if (!Canvas){ 
		UE_LOG(LogTemp, Warning, TEXT("Could not load Canvas properly from ABanditHUD::DrawButtons"));
		return; 
	}

	for (int i = 0; i < btnArray.Num(); i++){
		// Draw the button
		Canvas->SetDrawColor(FColor(255, 255, 255, 120));
		Canvas->DrawTile(btnArray[i].btnBackground,
			btnArray[i].xPos, btnArray[i].yPos, // start x/y coordinates of button
			btnArray[i].btnWidth, btnArray[i].btnHeight, // w/h of actual button
			btnArray[i].xPos, btnArray[i].yPos,  // texture start w/h
			btnArray[i].btnWidth, btnArray[i].btnHeight, // texture end w/h 
			BLEND_Translucent);

		// Draw Text in the button
		FCanvasTextItem buttonText(FVector2D(btnArray[i].xPos + btnArray[i].btnTextXPos, btnArray[i].yPos + btnArray[i].btnTextYPos),
			FText::FromString(btnArray[i].btnText),
			verdanaFont, btnArray[i].textColor);
		buttonText.Scale.Set(defaultFontScale, defaultFontScale);
		buttonText.bOutlined = false;
		Canvas->DrawItem(buttonText);
	}
}

// Checks to see if the cursor is in any of the buttons
int32 ABanditHUD::GetClickedButtonType(const TArray<FHUDButton>& btnArray){
	for (int32 i = 0; i < btnArray.Num(); i++){
		curCheckButton = &btnArray[i];

		// check if cursor is in bounds
		if (curCheckButton->xMin <= MouseLocation.X && MouseLocation.X <= curCheckButton->xMax &&
			curCheckButton->yMin <= MouseLocation.Y && MouseLocation.Y <= curCheckButton->yMax){
			// Active Button Type
			activeType = (int32)curCheckButton->buttonType;

			// Change Cursor
			bInButton = true;

			// Mouse Clicked?
			if (playerController->WasInputKeyJustPressed(EKeys::LeftMouseButton)){
				return activeType;
				// no need to check the rest of the buttons
			}
		}
	}

	// No Click Occured This Tick
	return -1;
}

// Check Buttons
void ABanditHUD::ExecuteButtonSelection(const TArray<FHUDButton>& btnArray){
	// Check Confirm Buttons
	clickedButtonType = GetClickedButtonType(btnArray);

	switch (clickedButtonType){
	case (int32)EButtonTypes::MM_Restart:{
		playerController->ConsoleCommand("RestartLevel");
		break;
	}
	case (int32)EButtonTypes::MM_Exit:{
		bConfirmDialog = true;
		break;
	}
	case (int32)EButtonTypes::CNFRM_YES:{
		playerController->ConsoleCommand("Exit");
		break;
	}
	case (int32)EButtonTypes::CNFRM_NO:{
		bConfirmDialog = false;
		return;
	}
	default:
		break;
	}
}

void ABanditHUD::CheckAllButtons(){
	if (bConfirmDialog){
		ExecuteButtonSelection(buttonsConfirm);

		// take focus away from all other buttons
		// this logic is likely outdated
		return;
	}

	ExecuteButtonSelection(buttonsMain);
}

void ABanditHUD::DrawCursor(){
	if (bInButton){
		if (!cursorHovering) { return; }
		Canvas->DrawTile(cursorHovering, 
				MouseLocation.X + cursorXPosOffset, MouseLocation.Y + cursorYPosOffset, 
				cursorWidth, cursorHeight, 
				cursorHoverTexXPos, cursorHoverTexYPos,
				cursorHoverTexWidth, cursorHoverTexHeight, 
				BLEND_Translucent);
	}
	else{
		if (!cursorStandard){ return; }
		Canvas->DrawTile(cursorStandard,
			MouseLocation.X + cursorXPosOffset, MouseLocation.Y + cursorYPosOffset,
			cursorWidth, cursorHeight,
			cursorStandardTexXPos, cursorStandardTexYPos,
			cursorStandardTexWidth, cursorStandardTexHeight,
			BLEND_Translucent);
	}
}

void ABanditHUD::PlayerInputChecks(){
	if (playerController->WasInputKeyJustPressed(EKeys::Escape)){
		//SetCursorMoveOnly(false);
		return;
	}
	if (playerController->WasInputKeyJustPressed(EKeys::F)){
		//SetCursorMoveOnly(banditController->IsLookInputIgnored());
		return;
	}
	if (playerController->WasInputKeyJustPressed(EKeys::H)){
		bDrawHUD = !bDrawHUD;
		return;
	}

	// Confirm
	if (bConfirmDialog){
		if (playerController->WasInputKeyJustPressed(EKeys::Y)){
			playerController->ConsoleCommand("Exit");
			// could replace with function based on confirm context
			return;
		}
		if (playerController->WasInputKeyJustPressed(EKeys::N)){
			bConfirmDialog = false;
			buttonsConfirm.Empty();

			return;
		}
	}
}

void ABanditHUD::Reset(){
	activeType = -1;
	bInButton = false;
}

void ABanditHUD::DocumentFoundAlert(AUsableActor* document){
	UE_LOG(YourLog, Warning, TEXT("Document is %s"), *document->GetName());
}

void ABanditHUD::DrawHUD(){
	// Controller checks for input and mouse cursor
	if (!playerController){
		// Attempt to Reacquire PC
		playerController = GetOwningPlayerController();

		if (!playerController) { return; }
	}

	// Player Input
	PlayerInputChecks();

	// Draw HUD?
	if (bDrawHUD) { return; }

	// Super
	Super::DrawHUD();

	// No Canvas?
	if (!Canvas) { return; }

	// Reset States
	Reset();

	// Get New Mouse Position
	/*playerController->GetMousePosition(MouseLocation.X, MouseLocation.Y);

	// Cursor In Buttons
	CheckAllButtons();

	// Draw Dialogs
	DrawDialogs();

	// Do Last
	// Draw Cursor

	DrawCursor();*/

	DrawMiniMap();
}

void ABanditHUD::Pause(){
	//Widget will not self-destruct unless the HUD's SharedPtr (and all other SharedPtrs) destruct first.
	SAssignNew(CitizenCluesHints, SCitizenCluesHints).OwnerHUD(this);

	//Pass viewport a weak ptr to widget
	if (GEngine->IsValidLowLevel())
	{
		GEngine->GameViewport->
			/*Viewport's weak ptr will not give Viewport ownership of Widget*/
			AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(CitizenCluesHints.ToSharedRef()));
	}

	if (CitizenCluesHints.IsValid())
	{
		//Set widget's properties as visible (sets child widget's properties recursively)
		CitizenCluesHints->SetVisibility(EVisibility::Visible);
	}

}


void ABanditHUD::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
	
}

void ABanditHUD::DrawMiniMap()
{
	//ABanditController* PC = Cast<ABanditController>(PlayerOwner);
	ABanditGameMode* MyGameState = GetWorld()->GetGameState<ABanditGameMode>();
	FCanvasTileItem MapTileItem(FVector2D(0.0f, 0.0f), FVector2D(0.0f, 0.0f), FLinearColor::White);
	const FRotationMatrix RotationMatrix(FRotator(0, 0, 0));
	MapTileItem.Texture = MiniMapTexture->Resource;
	MapTileItem.Size = FVector2D(MapWidth, MapHeight);
	MapTileItem.BlendMode = SE_BLEND_Opaque;
	Canvas->DrawItem(MapTileItem, FVector2D(Canvas->ClipX - MapWidth, Canvas->ClipY - MapHeight));
	FCanvasTileItem TileItem(FVector2D(0.0f, 0.0f), FVector2D(0.0f, 0.0f), FLinearColor::White);
	TileItem.Size = FVector2D(1 * UIScale, 1 * UIScale);
	FLinearColor DrawColor;
	FVector place;
	for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
	{
		ABanditController* BanditChar = Cast<ABanditController>(*Iterator);
		if (BanditChar != NULL){
			DrawColor = FColor(49, 137, 253, 255);
			place = BanditChar->GetActorLocation();
		}
		else{
			ACharacter* GuardAI = Cast<ACharacter>(*Iterator);
			DrawColor = FColor(98, 45, 198, 255);
			place = GuardAI->GetActorLocation();
		}

		TileItem.SetColor(DrawColor);

		//now make it so that it fits in the tiny minimap
		//float X = abs(float(place.X));
		//float Y = abs(float(place.Y));

		float X = place.X;
		float Y = place.Y;

		X /= WorldExtent.X;
		X *= MapWidth;
		Y /= WorldExtent.Y;
		Y *= MapHeight;

		float X2 = Canvas->ClipX - MapWidth/2 + X + OffsetMap.X;
		float Y2 = Canvas->ClipY - MapHeight/2 + Y + OffsetMap.Y;
		
		if (abs(X) > MapWidth/2 - 10 || abs(Y) > MapHeight/2 + 20){

		}
		else{
			Canvas->DrawItem(TileItem, X2, Y2);
		}
	}
	



}

