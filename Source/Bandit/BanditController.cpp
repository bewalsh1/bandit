// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "BanditController.h"
#include "BanditHUD.h"
#include "BanditGameMode.h"
#include "UsableActor.h"
#include "DocumentPickup.h"
#include "Kismet/GameplayStatics.h"
#include "SCitizenCluesHints.h"

ABanditController::ABanditController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Set variables for struggle meter
	CaptureBar = 0.f;
	MovementLocked = false;
	CanBeCaught = true;
	JailTeleNow = false;
	Escaped = false;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	PlayerLocation = FVector(0.f, 0.f, 0.f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	MusicAlertSphere = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("MusicAlertSphere"));
	MusicAlertSphere->InitSphereRadius(700.f);
	MusicAlertSphere->bGenerateOverlapEvents = true;
	MusicAlertSphere->SetCollisionProfileName("OverlapAll");
	MusicAlertSphere->OnComponentBeginOverlap.AddDynamic(this, &ABanditController::OnActorBeginOverlap);
	MusicAlertSphere->OnComponentEndOverlap.AddDynamic(this, &ABanditController::OnActorEndOverlap);
	MusicAlertSphere->AttachParent = RootComponent;

	hintSound = ObjectInitializer.CreateDefaultSubobject<UAudioComponent>(this, TEXT("Hint Sound"));
	hintSound->AttachParent = RootComponent;
	hintSound->bAutoActivate = false;

	// Set Variables for CitizenClues
	isPaused = false;
}

void ABanditController::PostInitializeComponents(){
	Super::PostInitializeComponents();

	if (GetWorld()){
		game = Cast<ABanditGameMode>(GetWorld()->GetAuthGameMode());
	}
}

void ABanditController::MoveForward(float Value){
	if ((Controller != NULL) && (Value != 0.0f) && (!MovementLocked)){
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ABanditController::MoveRight(float Value){
	if ((Controller != NULL) && (Value != 0.0f) && (!MovementLocked)){
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ABanditController::TurnAtRate(float Rate){
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABanditController::LookUpAtRate(float Rate){
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ABanditController::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location){
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1){
		Jump();
	}
}

void ABanditController::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location){
	if (FingerIndex == ETouchIndex::Touch1){
		StopJumping();
	}
}

void ABanditController::SetupPlayerInputComponent(class UInputComponent* InputComponent){
	// Set up gameplay key bindings
	check(InputComponent);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAction("Use", IE_Pressed, this, &ABanditController::Use);

	FInputActionBinding& toggle = InputComponent->BindAction("Pause", IE_Pressed, this, &ABanditController::SetPause);
	toggle.bExecuteWhenPaused = true;
	InputComponent->BindAxis("MoveForward", this, &ABanditController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABanditController::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ABanditController::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ABanditController::LookUpAtRate);

	// handle touch devices
	InputComponent->BindTouch(IE_Pressed, this, &ABanditController::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &ABanditController::TouchStopped);
}

void ABanditController::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
}

void ABanditController::BeginPlay(){
	Super::BeginPlay();
}

void ABanditController::Use(){
	/*if (GEngine){
		GEngine->AddOnScreenDebugMessage(0, 10.0f, FColor::White, TEXT("Inside Use Function"));
	}*/
	for (int i = 0; i < documents.Num(); i++){
		FVector distanceVector = this->GetActorLocation() - documents[i]->GetActorLocation();
		float distance = distanceVector.Size();
		if (distance < 150.f){
			// Play alert for finding document
			UGameplayStatics::PlaySoundAtLocation(this, foundMusic, this->GetActorLocation(), 1.f, 1.f, 0.f, NULL);

			// bring up alert for document pickup confirmation
			
			// load document information into UI
			ABanditHUD::DocumentFoundAlert(documents[i]);

			// destroy actor in level
			documents[i]->Destroy();

			// load next event  
			if (game){
				game->SpawnDocument();
			}
		}
	}
	
}

void ABanditController::OnActorBeginOverlap(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	/*if (GEngine){
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Inside Begin Overlap"));
	}*/
	if (OtherActor->IsA(AUsableActor::StaticClass())){
		// figure out some way to determine which component caused overlap
		// after adding document, set all actors in documents array to glow

		documents.AddUnique(Cast<AUsableActor>(OtherActor));
		hintSound->Play();
	}
}

void ABanditController::OnActorEndOverlap(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex){
	if (OtherActor->IsA(AUsableActor::StaticClass())){
		// if begin overlap component thing figured out, make sure to stop glow here

		hintSound->Stop();
		documents.Remove(Cast<AUsableActor>(OtherActor));
	}
}



void ABanditController::SetPause(){
	//Widget will not self-destruct unless the HUD's SharedPtr (and all other SharedPtrs) destruct first.
	SAssignNew(CitizenCluesHints, SCitizenCluesHints);
	APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));

	if (!isPaused){
		//Pass viewport a weak ptr to widget
		if (GEngine->IsValidLowLevel())
		{
			GEngine->GameViewport->
				/*Viewport's weak ptr will not give Viewport ownership of Widget*/
				AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(CitizenCluesHints.ToSharedRef()));
		}

		if (CitizenCluesHints.IsValid())
		{
			//Set widget's properties as visible (sets child widget's properties recursively)
			CitizenCluesHints->SetVisibility(EVisibility::Visible);
		}
		
		isPaused = true;
	
	}
	else {
		GEngine->GameViewport->
			/* Remove Widget from view */
			RemoveViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(CitizenCluesHints.ToSharedRef()));

		isPaused = false;
	}
	
	if (MyPlayer != NULL){
		MyPlayer->SetPause(isPaused);
	}
}

/*
	Need something for increasing rate of change for every guard in struggle
*/
void ABanditController::StruggleCaptureCheck(float changeRate){
	ChangeBarOverTime(changeRate);
	if (CaptureBar >= 100){
		JailTeleNow = true;
	}
}

void ABanditController::ChangeBarOverTime(float changeRate){
	CaptureBar += changeRate;
}
		
void ABanditController::StruggleOut(float changeRate){
	ChangeBarOverTime(changeRate);
	if (CaptureBar <= 0){
		Escaped = true;
	}
}

