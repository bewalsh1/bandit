// Fill out your copyright notice in the Description page of Project Settings.

#include "Bandit.h"
#include "Guard.h"


AGuard::AGuard(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){

}

void AGuard::PostInitializeComponents(){
	Super::PostInitializeComponents();
}

void AGuard::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
}

void AGuard::BeginPlay(){
	Super::BeginPlay();
}