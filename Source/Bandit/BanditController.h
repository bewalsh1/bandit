// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "SoundDefinitions.h"
#include "UsableActor.h"
#include "BanditGameMode.h"
#include "BanditController.generated.h"

/**
 * 
 */
UCLASS()
class BANDIT_API ABanditController : public ACharacter{
	GENERATED_BODY()
	
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	virtual void BeginPlay() override;

public:
	ABanditController(const FObjectInitializer& ObjectInitializer);

	//Current location of Bandit, used in updating minimap, to be set as he moves
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Minimap)
	FVector PlayerLocation;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	virtual void Tick(float DeltaSeconds) override;

	USphereComponent* MusicAlertSphere;

	UFUNCTION()
	void OnActorBeginOverlap(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnActorEndOverlap(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Audio Component")
	UAudioComponent* hintSound;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Audio Component")
	UAudioComponent* foundSound;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Audio Component")
	UAudioComponent* guardFoundSound;

	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundCue* hintMusic;
	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundCue* foundMusic;
	UPROPERTY(EditDefaultsOnly, Category = "Audio")
	USoundCue* guardFoundMusic;

	TArray<AUsableActor*> documents;

	ABanditGameMode* game;

	//Create a bar that will fill and lead to capture
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grabbed)
	float CaptureBar;

	//Create a bool that will determine if you can move, true = can't move
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Grabbed)
	bool MovementLocked;

	//Create a bool that will determine if you have been grabbed recently
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Grabbed)
	bool CanBeCaught;

	//Create a bool that will determine if you have been grabbed recently
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Grabbed)
	bool JailTeleNow;

	//Create a bool that will determine if you have escaped being grabbed
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Grabbed)
	bool Escaped;

protected:
	//Raise or lower bar by blueprint determined rate, at 100, go to jail, at 0, escape guards
	UFUNCTION(BlueprintCallable, Category = Grabbed)
	void ChangeBarOverTime(float changeRate);

	// Maintains logic for breaking free of struggle system
	UFUNCTION(BlueprintCallable, Category = Grabbed)
	void StruggleOut(float changeRate);

	// Maintains logic for getting caught in struggle system
	UFUNCTION(BlueprintCallable, Category = Grabbed)
	void StruggleCaptureCheck(float changeRate);

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	// after all game elements are created
	virtual void PostInitializeComponents() override;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	// Use the actor currently in view (if derived from UsableActor)
	UFUNCTION(BlueprintCallable, Category = PlayerAbility)
	virtual void Use();

	// Pause the game and open Citizen Clues Hints
	UFUNCTION()
	virtual void SetPause();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = PauseMenu)
		bool isPaused;

	// Slate Widgets
	TSharedPtr<class SCitizenCluesHints> CitizenCluesHints;

	UFUNCTION(BlueprintImplementableEvent, Category = GameWon)
	void LoadGameWonScreen();

};