// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BanditHUD.h"
#include "BanditController.h"
#include "SlateBasics.h"


/**
 * 
 */
class BANDIT_API SCitizenCluesHints : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SCitizenCluesHints)
	{}
	SLATE_ARGUMENT(TWeakObjectPtr<class ABanditHUD>, OwnerHUD)
		SLATE_ARGUMENT(TWeakObjectPtr<class ABanditController>, OwnerController)

	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	SCitizenCluesHints();

	UFUNCTION(BlueprintCallable, Category = PauseMenu)
		const void ContinueButton();

	DECLARE_DELEGATE(ContinueButtonDelegate);
private:
	// Weak Pointer to our parent HUD
	TWeakObjectPtr<class ABanditHUD> OwnerHUD;
	TWeakObjectPtr<class ABanditController> OwnerController;

};
