// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/StaticMeshActor.h"
#include "UsableActor.generated.h"

/**
 * 
 */
UCLASS()
class BANDIT_API AUsableActor : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:
	AUsableActor(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintImplementableEvent)
	bool OnUsed(ACharacter* character);
	
	UFUNCTION(BlueprintImplementableEvent)
	bool StartFocusItem();

	UFUNCTION(BlueprintImplementableEvent)
	bool EndFocusItem();

	USphereComponent* overlapSphere;

	AStaticMeshActor* documentMesh;
};

static FORCEINLINE FName GetObjPath(const UObject* Obj){
	if (!Obj) return NAME_None;
	if (!Obj->IsValidLowLevel()) return NAME_None;

	FStringAssetReference path = FStringAssetReference(Obj);

	if (!path.IsValid()) return NAME_None;

	// The Class FString Name for this Object
	FString objString = Obj->GetClass()->GetDescription();

	objString += "'";
	objString += path.ToString();
	objString += "'";

	return FName(*objString);
}

template <typename ObjClass>
static FORCEINLINE ObjClass* LoadObjFromPath(const FName& path){
	if (path == NAME_None) return NULL;

	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(), NULL, *path.ToString()));
}

static FORCEINLINE UStaticMesh* LoadMeshFromPath(const FName& path){
	if (path == NAME_None) return NULL;

	return LoadObjFromPath<UStaticMesh>(path);
}
